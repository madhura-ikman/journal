<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::match(array('GET','POST'), '/journal', 'journalController@liveAd');
//Route::match(array('GET','POST'), '/cargills', 'cargillsController@liveAd');
//Route::match(array('GET','POST'), '/slip', 'slipController@liveAd');
Route::match(array('GET','POST'), '/summary', 'summaryController@summary');
Route::match(array('GET','POST'), '/search', 'summaryController@search');

Route::post('/callaction', 'summaryController@callactions');

//Route::match(array('GET','POST'), '/sellfast', 'sellfastController@liveAd');

//Route::get('users', function (){
//   return view('users');
//});

//Route::get('users',['uses'=>'UserController@index']);
//Route::get('glogin',array('as'=>'glogin','uses'=>'UserController@googleLogin')) ;
//Route::get('authentication','UserController@authentication') ;

Route::get('/glogin',array('as'=>'/glogin','uses'=>'UserController_fileUpload@googleLogin')) ;
Route::get('/authentication','UserController_fileUpload@authentication') ;
