<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class fosController extends Controller
{
    public function liveAd(Request $request)
    {
        $request->session()->put('path', '/journal');
        $inputs = Input::all();
        if (session()->get('email') == null) {

            return redirect('/authentication');
        }
        if ($request->session()->get('access') == 1) {

        } else {
            return redirect('/');
        }
        if ($request->session()->get('urole') == 5 || $request->session()->get('urole') == 12 || $request->session()->get('urole') == 10 || $request->session()->get('urole') == 11) {
            if (Input::has('submit')) {
                return redirect('/journal');
            }
            return view('fosLive');
        } else {
            return view('getAccess');
        }
    }
}
