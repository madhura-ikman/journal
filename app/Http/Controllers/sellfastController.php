<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class sellfastController extends Controller
{
    public function liveAd(Request $request){
        $request->session()->put('path','/sellfast');
        $inputs  = Input::all();
        if(session()->get('email')==null){

            return redirect('/authentication');
        }
        if($request->session()->get('access')==1){

        }else{
            return redirect('/');
        }
        if($request->session()->get('urole')==4 || $request->session()->get('urole')==12 || $request->session()->get('urole')==9 || $request->session()->get('urole')==11){
            if(Input::has('submit')){
                return redirect('/sellfast');
            }
            return view('sellfastLive');
        }else{
            return view('getAccess');
        }
    }
}
