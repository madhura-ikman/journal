<?php

namespace App\Http\Controllers;
use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
class UserController_fileUpload extends Controller
{


    public function googleLogin(Request $request)  {
        $google_redirect_url = route('/glogin');
        $gClient = new \Google_Client();
        $gClient->setApplicationName('ICompare');
        $gClient->setClientId('243373420608-ak0u664u3sh5hf2gqsl78eg816gvhlsc.apps.googleusercontent.com');
        $gClient->setClientSecret('kq9DrrWaVAWU_N9z3FziHRAr');
        $gClient->setRedirectUri($google_redirect_url);
        //  $gClient->setDeveloperKey(config('services.google.api_key'));
        $gClient->setScopes(array(
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
        ));
        //dd($gClient);
        $google_oauthV2 = new \Google_Service_Oauth2($gClient);
        //dd($google_oauthV2);
        if ($request->get('code')){

            $gClient->authenticate($request->get('code'));

            $request->session()->put('token', $gClient->getAccessToken());
        }

        if ($request->session()->get('token'))
        {
            $gClient->setAccessToken($request->session()->get('token'));
        }

        if ($gClient->getAccessToken()) {
            // dd($gClient->getAccessToken());
            //For logged in user, get details from google using access token
            $guser = $google_oauthV2->userinfo->get();


            $user = new User();
            $path = $request->session()->get('path');

            if ($user = User::where('email','=',$guser['email'])->first()) {
                //dd($user['access']);
                $request->session()->put('email', $guser['email']);
                $request->session()->put('access', $user['access']);
                $request->session()->put('urole', $user['users_role']);
                //dd(json_encode($request->session()->get('access')));
                return redirect($path);

            } else {
                $user = new User();
                $user->name = $guser['name'];
                $user->email = $guser['email'];
                $user->remember_token = $guser['id'];
                $user->password = "";
                $user->save();
                return view('getAccess');

            }
        }
        else
        {
            $authUrl = $gClient->createAuthUrl();
            return redirect()->to($authUrl);
        }

    }
    public function authentication(Request $request){

        return view('Google_Authentication_file_upload');
    }


}
