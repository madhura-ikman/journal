<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class summaryController extends Controller
{
    public function summary(Request $request)
    {
        $request->session()->put('path', '/summary');
        $inputs = Input::all();
        if (session()->get('email') == null) {

            return redirect('/authentication');
        }
        if ($request->session()->get('access') == 1) {

        } else {
            //return redirect('/');
            return view('getAccess');
        }
        if ($request->session()->get('urole') == 5 || $request->session()->get('urole') == 12 || $request->session()->get('urole') == 10 || $request->session()->get('urole') == 11) {
            if (Input::has('submit')) {
                return redirect('/summary');
            }
            return view('onlineLive');
        } else {
            return view('getAccess');
        }
    }
    public function search(){
        return view('search');
    }
    public function callactions(){
        return view('onlineLive');
    }
}
