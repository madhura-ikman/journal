<!doctype html>
<html lang="en">
<header>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css"
          integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="shortcut icon" href="http://vms.ikman.cloud/assets/images/favicon-ikman.ico">
    <link rel="stylesheet" href={{asset('css/style.css')}}>
</header>
<body>
<div class="container">
    <div class="custom-container">
        <div class="panel" style="background: transparent !important;box-shadow:none;border: none">
            <div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="{{ route('/glogin') }}">
                            <img style="width: 100%" src={{asset('img/google.png')}} />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <p class="text-center" style="color: #009877">Copyright 2018 ikman.lk All right reserved.</p>
    </div>
</div>
</body>
</html>