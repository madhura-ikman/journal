<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>
<body>

<form action="" method="post">

    <table>
        <tr>
            <td><input type="text" name="txtpayID" placeholder="Pay ID"  value="<?php isset($_POST['txtpayID'])?? "" ?>" /></td>
        </tr>
        <tr>
            <td><input type="text" name="txtpaysysref" placeholder="Card No" /></td>
        </tr>
        <tr>
            <td><input type="text" name="txtreference" placeholder="Reference No" /></td>
        </tr>
        <tr>
            <td>
                <select name="txtstatus">
                    <option value="" >Status</option>
                    <option value="100">Payment Pending</option>
                    <option value="1">Payment Success</option>
                    <option value="2">Failed With Reason</option>
                    <option value="3">Failed Without Reason</option>
                    <option value="4">Partial Payment</option>
                    <option value="6">To be refunded</option>
                    <option value="7">Refund</option>
                    <option value="8">Collected</option>
                    <option value="9">Discount</option>
                </select>

            </td>
        </tr>
        <tr>
            <td><input type="text" name="txtsdate" placeholder="Start Date" /></td>
        </tr>
        <tr>
            <td><input type="text" name="txtedate" placeholder="End Date" /></td>
        </tr>
        <tr>
            <td>
                <select name="txtportal">
                    <option value="">Portal</option>
                    <option value="listing">Listing</option>
                    <option value="member">Member</option>
                    <option value="compare">Compare</option>
                    <option value="buynow">Buy now</option>
                    <option value="voucher">Voucher</option>
                    <option value="sellfast">Sell Fast</option>
                    <option value="sandbox">Sandbox</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <select name="txtmethod">
                    <option value="">Payment Method</option>
                    <option value="100">"OPM-Sampath-VISA"</option>
                    <option value="101">OPM-Sampath-Master</option>
                    <option value="102">OPM-NTB-AmEx</option>
                    <option value="103">UPAY-JustPay</option>
                    <option value="104">UPAY-Card-Visa-</option>
                    <option value="105">UPAY-Card-Master</option>
                    <option value="106">Pay-N-Go-Cash</option>
                    <option value="107">Cargills-POS</option>
                    <option value="108">OPM-SlipUpload</option>
                    <option value="202">Mapper-Cash</option>

                </select>
            </td>
        </tr>
        <tr>
            <td><input type="submit" name="SubmitButton" value="Search" /></td>
        </tr>
    </table>
    {!! Form::token() !!}
</form>
<?php
try {
    $dbh = new PDO("mysql:dbname=code_gen;host=52.187.134.42;port=6603","reader","LqSS5Zn9cM%y!D_X");
}catch(PDOException $e) {
    die($e);
}



if(isset($_POST['SubmitButton'])){
    $PayID=$_POST['txtpayID'];
    $reference=$_POST['txtreference'];
    $status=$_POST['txtstatus'];
    $sdate=$_POST['txtsdate'];
    $edate=$_POST['txtedate'];
    $portal=$_POST['txtportal'];
    $method=$_POST['txtmethod'];
    $paysysref=$_POST['txtpaysysref'];


    $query= "SELECT jn.code as 'Pay ID',jn.portal_ref as 'reference',jn.created_at,jn.updated_at,jn.version,jn.portal,jn.amount,jn.pay_sys_ref,jn.payer,ps.status_desc as 'status',pm.payment_method, pm.payment_system, pf.fail as 'reason' FROM code_journal jn join payment pm on jn.payment_id = pm.idpayment join pay_status ps on jn.settled = ps.status_code LEFT JOIN pay_fail pf on jn.id = pf.code_journal_id WHERE 1=1";

    if($PayID){
        $query .=" AND code like '%".$PayID."%'";
    }
    if($reference){
        $query.=" AND portal_ref like '%".$reference."%'";
    }
    if($status){

        if($status==100){
            $status=0;
        }
        $query.=" AND jn.settled = ".$status."";

    }
    if($sdate AND !$edate){
        $query.= " AND jn.updated_at like '%".$sdate."%'";
    }
    if($portal){
        $query.= " AND jn.portal like '%".$portal."%'";
    }
    if($method){
        $query.= " AND pm.idpayment like '%".$method."%'";
    }
    if($paysysref){
        $query.= " AND jn.pay_sys_ref like '%".$paysysref."%'";
    }
    if($sdate AND $edate){
        $query.= " AND jn.updated_at BETWEEN '".$sdate."' AND '".$edate."' ORDER BY jn.updated_at ASC limit 1500";
    }
        $query.= " ORDER BY jn.id DESC limit 250";

}
else {
    $query = "SELECT jn.code as 'Pay ID',portal_ref as 'reference',jn.created_at,jn.pay_sys_ref,jn.updated_at,jn.version,jn.portal,jn.amount,jn.payer,ps.status_desc as 'status',pm.payment_method, pm.payment_system FROM code_journal jn join payment pm on jn.payment_id = pm.idpayment join pay_status ps on jn.settled = ps.status_code ORDER BY `id` DESC limit 250";
}

$tableheader = false;
$sth = $dbh->prepare($query);

if(!$sth->execute()) {
    die('Error');
}

echo "<table>";

while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
    if($tableheader == false) {
        echo '<tr>';
        foreach($row as $key=>$value) {
            echo "<th>{$key}</th>";
        }
        echo '</tr>';
        $tableheader = true;
    }
    echo "<tr>";
    foreach($row as $value) {
        echo "<td>{$value}</td>";
    }
    echo "</tr>";
}
echo "</table>";
$dbh = null;
?>
</body>
</html>