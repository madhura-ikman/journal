<!DOCTYPE html>
<html lang="en">
<head>
    <title>Payment Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <style>
        .box {
            float: center;
            background-color: #009877;
            width: 400px;
            border: 25px gray;
            padding: 25px;
            margin: 25px;
            height: 150px;
            border: none;
            color: white;
            padding: 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 18px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 12px;
        }
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
            background-color:#007168;
        }

        .button {

            float: center;
            background-color: #009877;
            width: 400px;
            border: 25px gray;
            padding: 25px;
            margin: 25px;
            height: 150px;
            border: none;
            color: white;
            padding: 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 18px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 12px;
        }

        .button:hover {
            font-size: 26px;
            background-color: #007168;
            color: white;
        }


    </style>
</head>
<body  onload="startTime();"r>
<?php
$date = date("Y-m-d");
?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid" style="color: white; font-size: 18px; float: right">
        <?php echo $date; ?>
        <div id="txt" ></div>
    </div>
    <h1><span style="color: white; padding-left: 10px;"><img src="https://pbs.twimg.com/profile_images/512125430183124993/ZF86ePP2_400x400.png" height="100px"> </span><span style="color: white; padding-left: 10px; padding-top: 50px ">IKMAN PAY DASHBOARD</span></h1>
    <br/>
</nav>

<div class="jumbotron">
    <div class="container text-center">
        <form class="form-inline" action="{{ url('/callaction') }}" method="POST">
            <div class="form-group">
                <input type="text" class="form-control" id="sdate" placeholder="Enter Start date" value="<?php echo $startdate; ?>" name="txtstartdate" required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="edate" placeholder="Enter End date" value="<?php echo ($enddate); ?>" name="txtenddate" >
            </div>
            <div class="form-group">
                <select class="form-control" name="portal" required="">
                    <option value="">Portal</option>
                    <option <?=$portal == 'listing' ? ' selected="selected"' : '';?> value="listing">Listing</option>
                    <option <?=$portal == 'member' ? ' selected="selected"' : '';?> value="member">Member</option>
                    <option <?=$portal == 'compare' ? ' selected="selected"' : '';?> value="compare">Compare</option>
                    <option <?=$portal == 'buynow' ? ' selected="selected"' : '';?> value="buynow">Buy now</option>
                    <option <?=$portal == 'voucher' ? ' selected="selected"' : '';?> value="voucher">Voucher</option>
                    <option <?=$portal == 'sellfast' ? ' selected="selected"' : '';?> value="sellfast">Sell Fast</option>
                    <option <?=$portal == 'sandbox' ? ' selected="selected"' : '';?> value="sandbox">Sandbox</option>
                </select>
            </div>
            <input type="submit" name="btnsearch" value="Search" type="submit" class="btn btn-default">
            {!! Form::token() !!}
        </form>
        <div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $( function() {
            $( "#sdate" ).datepicker({ dateFormat: 'yy-mm-dd' });
            $( "#edate" ).datepicker({ dateFormat: 'yy-mm-dd' });
        });
    });

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }

</script>

<form class="form-inline" action="{{ url('/search') }}" method="POST" target=”_blank”>
    <?php
    if($startdate!= ""){
        $payment=="false";
    }
    ?>
    <input type="hidden" value="<?php echo $startdate; ?>" name="txtstartdate">
    <input type="hidden" value="<?php echo $enddate; ?>" name="txtenddate">
    <input type="hidden" value="<?php echo $portal; ?>" name="portal">
    <div class="container-fluid bg-3 text-center">
        <div class="row">
            <div class="col-sm-4">

                <button name="btnsuccess" class="button"
                        <?php
                        if($payment=="true"){
                        ?>
                        disabled="";
                <?php
                    }
                    ?>
                ><?php echo("SUCCESS TRANSACTIONS"); ?><br/><span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo($success_no_of_transactions); ?></span></button>


            </div>
            <div class="col-sm-4">
                <button type="submit" class="button" disabled=""> <?php echo("PENDING TRANSACTIONS"); ?> <br/> <span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo($pending_no_of_transactions); ?></span></button>

            </div>

            <div class="col-sm-4">
                <button class="button" name="btnfailed"
                        <?php
                        if($payment=="true"){
                        ?>
                        disabled="";
                <?php
                    }
                    ?>
                > <?php echo("FAILED TRANSACTIONS");?><br/><span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo($failed_no_of_transactions);?></span> </button>

            </div>
        </div>
    </div><br>

    <div class="container-fluid bg-3 text-center">
        <div class="row">
            <div class="col-sm-4">
                <button type="submit" class="button" name="btnsuccess"
                        <?php
                        if($payment=="true"){
                        ?>
                        disabled="";
                <?php
                    }
                    ?>
                ><?php echo("REVENUE"); ?><br/><span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo("Rs.".$success_revenue); ?></span></button>
            </div>

            <div class="col-sm-4">
                <button class="button" disabled=""><?php echo("PENDING REVENUE");?><br/><span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo("Rs.".$pending_revenue); ?></span></button>
            </div>
            <div class="col-sm-4">
                <button class="button" name="btnfailed"
                        <?php
                        if($payment=="true"){
                        ?>
                        disabled="";
                <?php
                    }
                    ?>
                ><?php echo("REVENUE OF FAILED TRANSACTIONS"); ?><br/><span style="color:#424E4E; font-weight: bold; font-size: 25px"><?php echo("Rs.".$failed_revenue); ?></span></button>

            </div>
        </div>
    </div><br>

    </div><br><br>

    <table class="table">
        <thead>
        <tr>
            <th>System</th>
            <th>Method</th>
            <th>Success Count</th>
            <th>Failed Count</th>
        </tr>
        </thead>
        <tbody>
        <?php

        while($row = $success->fetch(PDO::FETCH_ASSOC)) {
        $available=false;?>

        <tr class="success">
            <?php

            echo "<td>".($row["payment_system"])."</td>";
            echo "<td>".($row["payment_method"])."</td>";
            echo "<td>".($row["count(cj.payment_id)"])."</td>";
            $id=$row["payment_id"];

            for( $x=0; $x<sizeof($results); $x++){
            if($results[$x]["payment_id"]==$id){
            $available=true;
            ?>
            <td class="danger"><?php echo ($results[$x]["count(cj.payment_id)"]);

                ?></td>
            <?php break;
            }

            }

            if($available==false){
            ?>
            <td class="danger"></td>


            <?php  }

            ?>

        </tr>
        <?php }

        ?>

        </tbody>
    </table>


        {!! Form::token() !!}
</form>
</body>
</html>
